"""
“Copyright 2020 L'esfera”

This file is part of L'esfera.

L'esfera is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from fastapi import Depends, FastAPI, HTTPException

from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder

from sqlalchemy.orm import Session
from persistence import crud, models, schemas
from persistence.database import SessionLocal, engine

from pprint import pprint as pp

tags_metadata = [
    {
        "name": "nodes",
        "description": "Operations with nodes.",
    },
    {
        "name": "relationships",
        "description": "Relationships join nodes together.",
    },
    {
        "name": "comment",
        "description": "A relationship can have a comment.",
    },
]


app = FastAPI(
    title="L'esfera",
    description="No nonsense mind map",
    version="1.0",
    openapi_tags=tags_metadata
)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

origins = [
    "http://localhost",
    "http://127.0.0.1",
    #"*" #(for development)
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/nodes",  tags=["nodes"])
def get_nodes(db: Session = Depends(get_db)):
    """
    Returns a dictionary. Keys are node names. Values are node ids.
    """
    return crud.get_nodes(db=db)


@app.get("/nodes/{node_id}", response_model=schemas.Node, tags=["nodes"])
def get_node(node_id: int, db: Session = Depends(get_db)):
    db_node = crud.get_node(db, node_id=node_id)
    if db_node is None:
        raise HTTPException(status_code=404, detail="Node not found")
    return db_node


@app.post("/nodes", response_model=schemas.Node, tags=["nodes"])
def create_node(node: schemas.NodeCreate, db: Session = Depends(get_db)):
    return crud.create_node(db=db, node=node)


@app.patch("/nodes/{node_id}", response_model=schemas.NodeBase, tags=["nodes"])
def update_node(node_id: int, node: schemas.NodeDesc, db: Session = Depends(get_db)):
    db_node = crud.get_node(db, node_id=node_id)
    if db_node is None:
        raise HTTPException(status_code=404, detail="Node not found")
    if db_node.description != node.description:
        prev_edition = db_node.make_history()
        db.add(prev_edition)
        db_node.description = node.description
        db.commit()
    return db_node


@app.get("/relationships/{node_id}", response_model=schemas.RelatedNodes, tags=["relationships"])
def get_related_nodes(node_id: int, db: Session = Depends(get_db)):
    result = crud.get_related_nodes(db=db, node_id=node_id)
    #pp(result)
    return result


@app.post("/relationships", response_model=schemas.RelatedNodes, tags=["relationships"])
def add_link_to_node(nodes: schemas.Node2NodeIDs, db: Session = Depends(get_db)):
    result = crud.add_related_node(db=db, nodes=nodes)
    #pp(result)
    return result


@app.post("/comments", response_model=schemas.RelatedNodes, tags=["comments"])
def update_comment(node2nodeComment: schemas.Comment, db: Session = Depends(get_db)):
    result = crud.update_node_to_node_comment(db=db, comment_data=node2nodeComment)
    #pp(result)
    return result


@app.get("/node-weights")
def node_weights(db: Session = Depends(get_db)):
    return {
            "weights": crud.get_node_weights(db, limit=50),
            "total_nodes": db.query(models.Node).count()
            }


@app.get("/stats")
def get_stats(db: Session = Depends(get_db)):
    total_nodes = db.query(models.Node).count()
    last_node = crud.get_node(db, node_id=total_nodes)
    last_node = {
        'id': last_node.id,
        'name': last_node.name,
        'description': last_node.description,
        'created': last_node.created
    }
    return  { 'total_nodes' : total_nodes, 'last_node': last_node }
