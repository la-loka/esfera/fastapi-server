"""
“Copyright 2020 L'esfera”

This file is part of L'esfera.

L'esfera is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys
from persistence.database import SessionLocal, engine
from persistence import models


models.Base.metadata.create_all(bind=engine)

def seed_database():
    try:
        node_name = sys.argv[1]
    except:
        print("Please define the name of the first node")
        return
    db = SessionLocal()
    first_node = db.query(models.Node).filter(models.Node.id == 1).first()
    if not first_node:
        first_node = models.Node(name=node_name, description="A description")
        db.add(first_node)
        db.commit()
        print("Database created with first node: {}".format(first_node.name))
    else:
        print("Sorry, the database already exists")
        print("First node was named: {}".format(first_node.name))
    db.close()

seed_database()
