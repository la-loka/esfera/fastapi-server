
# Server Installation

L'esfera has two parts.
* The server (this document). Written with the FastAPI python framework.
* The frontend. Written in Vuejs2

## Install the server

### Get the source code

You can download a gzip'd tar file
```bash
cd /opt
wget https://gitlab.com/la-loka/esfera/fastapi-server/-/archive/master/fastapi-server-master.tar.gz
mkdir esfera-server
tar zxvf fastapi-server-master.tar.gz --strip-components=1 -C esfera-server
```
or clone the git repository
```bash
cd /opt
git clone https://gitlab.com/la-loka/esfera/fastapi-server.git esfera-server
or ..
git clone git@gitlab.com:la-loka/esfera/fastapi-server.git esfera-server
```

### Install the source code

You may need to install
```bash
apt-get install python3-dev gcc make
```

```bash
cd /opt/esfera-server
python3 -m venv ./venv
source ./venv/bin/activate
pip install -e ./
```

### Create the database

To seed the database you need to set the name of the first node manually.

What is the name of the first node? example: `Our project`

```bash
cd /opt/esfera-serve
source venv/bin/activate
python create_db.py "Our project"
```

## Development

```bash
uvicorn main:app --reload --port 5050
```

## Production

### Test gunicorn

```bash
cd /opt/esfera-serve
source venv/bin/activate
gunicorn --bind 127.0.0.1:5050 -w 4 -k uvicorn.workers.UvicornWorker main:app
```

### Create a service

Create a user `esfera` to run the esfera app
```
adduser --shell=/bin/false --no-create-home --disabled-password esfera
chown -R esfera.esfera /opt/esfera-server
```

Create and edit `/etc/systemd/system/esfera.service`
```
[Unit]
Description = L'esfera
After = network.target

[Service]
PermissionsStartOnly = true
PIDFile = /run/esfera/esfera.pid
User = esfera
Group = esfera
WorkingDirectory = /opt/esfera-server
ExecStartPre = /bin/mkdir /run/esfera
ExecStartPre = /bin/chown -R esfera:esfera /run/esfera
ExecStart = /opt/esfera-server/venv/bin/gunicorn main:app -b 127.0.0.1:5050  -w 4 -k uvicorn.workers.UvicornWorker --pid /run/esfera/esfera.pid
ExecReload = /bin/kill -s HUP $MAINPID
ExecStop = /bin/kill -s TERM $MAINPID
ExecStopPost = /bin/rm -rf /run/esfera
PrivateTmp = true

[Install]
WantedBy = multi-user.target
```

```
chmod 755 /etc/systemd/system/esfera.service
systemctl daemon-reload
```


# Backups

L'esfera uses SQLite. The database is contained in one single file.

To backup the database, copy the file `esfera.db` to somewhere safe.
