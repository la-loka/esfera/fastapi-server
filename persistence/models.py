"""
“Copyright 2020 L'esfera”

This file is part of L'esfera.

L'esfera is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# https://docs.sqlalchemy.org/en/14/orm/join_conditions.html#self-referential-many-to-many

import datetime

from sqlalchemy import Table, UniqueConstraint
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship

from .database import Base


node_to_node = Table("node_to_node", Base.metadata,
    Column('id', Integer, primary_key=True, index=True, autoincrement=True),
    Column("left_node_id", Integer, ForeignKey("node.id"), index=True),
    Column("right_node_id", Integer, ForeignKey("node.id"), index=True),
    UniqueConstraint('left_node_id', 'right_node_id', name='unique_node2node'),
    Column("comment_id", Integer, ForeignKey("comment.id")),
    Column("value", Integer, default=1),
    Column("created", DateTime, default=datetime.datetime.utcnow)
    )


class Node(Base):
    __tablename__ = "node"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(250), unique=True, index=True)
    description = Column(String, nullable=False)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    history = relationship("NodeHistory", back_populates="node")
    #related = []

    related_nodes = relationship('Node',
                        secondary=node_to_node,
                        primaryjoin=id==node_to_node.c.left_node_id,
                        secondaryjoin=id==node_to_node.c.right_node_id)


    def make_history(self):
        edition = NodeHistory(  name=self.name,
                                description=self.description,
                                node_id = self.id
                                )
        #print ("{} {}".format(edition.name, edition.description))
        return edition
        

class NodeHistory(Base):
    __tablename__ = "node_history"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(250))
    description = Column(String, nullable=False)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    node_id = Column(Integer, ForeignKey('node.id'))
    node = relationship("Node", uselist=False, back_populates="history")
    
    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.description = kwargs['description']
        self.node_id = kwargs['node_id']


class Comment(Base):
    __tablename__ = "comment"

    id = Column(Integer, primary_key=True, index=True)
    text = Column(String, nullable=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    history = relationship("CommentHistory", back_populates="comment")
    
    def make_history(self):
        edition = CommentHistory(text = self.text, comment_id = self.id)
        #print ("{} {}".format(edition.name, edition.description))
        return edition
        
        
class CommentHistory(Base):
    __tablename__ = "comment_history"

    id = Column(Integer, primary_key=True, index=True)
    text = Column(String, nullable=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    comment_id = Column(Integer, ForeignKey('comment.id'))
    comment = relationship("Comment", uselist=False, back_populates="history")
    
    def __init__(self, **kwargs):
        self.text = kwargs['text']
        self.comment_id = kwargs['comment_id']
