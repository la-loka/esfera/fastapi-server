"""
“Copyright 2020 L'esfera”

This file is part of L'esfera.

L'esfera is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import List, Dict, Optional
from pydantic import BaseModel, Field, constr


class NodeBase(BaseModel):
    name: constr(max_length=250)
    #name: str
    description: str
    class Config:
        orm_mode = True

class NodeDesc(BaseModel):
    description: str
 
class NodeCreate(NodeBase):
    left_node_id: int
    class Config:
        orm_mode = True

class Node(NodeBase):
    id: int
    related: List

    #class Config:
    #    orm_mode = True

class Nodes(BaseModel):
    pass
    
    class Config:
            schema_extra = {
                "example": {
                    "node_name": 1,
                }
            }


class RelatedNodes(BaseModel):
    related_nodes: List

class Node2NodeIDs(BaseModel):
    left_node_id: int
    right_node_id: int

class Comment(Node2NodeIDs):
    comment: str

"""
class LinkedNodes(BaseModel):
    links: List
"""
