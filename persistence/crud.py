"""
“Copyright 2020 L'esfera”

This file is part of L'esfera.

L'esfera is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sqlalchemy import select, update, and_, func
from sqlalchemy.orm import Session

from . import models, schemas


def get_nodes(db: Session, skip: int = 0, limit: int = 100):
    #return db.query(models.Node).offset(skip).limit(limit).all()
    nodes = db.query(models.Node).all()
    result = {}
    for node in nodes:
        result[node.name] = node.id
    return result


def get_node(db: Session, node_id: int):
    db_node = db.query(models.Node).filter(models.Node.id == node_id).first()
    db_node.related=get_related_nodes(db, db_node)
    return db_node


def create_node(db: Session, node: schemas.NodeCreate):
    related_node = db.query(models.Node).filter(models.Node.id == node.left_node_id).first()
    if not related_node:
        return

    db_node = models.Node(name=node.name, description=node.description)
    db.add(db_node)
    db.flush()

    create_relationship(db, db_node, related_node)
    db.commit()
    #db.refresh(db_node)
    db_node.related=get_related_nodes(db, db_node)
    return db_node


def add_related_node(db: Session, nodes: schemas.Node2NodeIDs):
    #print("{} - {}".format(nodes.left_node_id, nodes.right_node_id))
    
    left_node = db.query(models.Node).filter(models.Node.id == nodes.left_node_id).first()
    right_node = db.query(models.Node).filter(models.Node.id == nodes.right_node_id).first()
    
    if not (left_node or right_node):
        return {'related_nodes': []}
        
    if not create_relationship(db, left_node, right_node):
        inc_node_to_node_value(db, left_node.id, right_node.id)
    
    db.commit()
    return {'related_nodes': get_related_nodes(db, left_node)}


def get_related_nodes(db: Session, db_node):
    result = []
    for related_node in db_node.related_nodes:
        node_2_node_data = get_node_to_node_data(db, db_node.id, related_node.id)
        result.append({
                'id': related_node.id,
                'name': related_node.name,
                'description': related_node.description,
                'value': node_2_node_data['value'],
                'comment': node_2_node_data['comment'],
        })
    return result


def create_relationship(db, left_node, right_node):
    if right_node in left_node.related_nodes:
        return False
    left_node.related_nodes.append(right_node)
    right_node.related_nodes.append(left_node)
    comment = models.Comment()
    db.add(comment)
    db.flush()

    up = update(models.node_to_node) \
        .where(
            and_(
                models.node_to_node.c.left_node_id == left_node.id,
                models.node_to_node.c.right_node_id == right_node.id
            )) \
        .values( comment_id = comment.id )
    db.execute(up)
    up = update(models.node_to_node) \
        .where(
            and_(
                models.node_to_node.c.left_node_id == right_node.id,
                models.node_to_node.c.right_node_id == left_node.id
            )) \
        .values( comment_id = comment.id )
    db.execute(up)   
    return True


def get_node_to_node_data(db: Session, left_node_id: int, right_node_id: int):
    s = select([models.node_to_node.c.value, models.node_to_node.c.comment_id]) \
        .where(
            and_(
                models.node_to_node.c.left_node_id == left_node_id,
                models.node_to_node.c.right_node_id == right_node_id
        ))
    result = db.execute(s).fetchone()
    try:
        comment_id = result[1]
        comment = db.query(models.Comment).filter(models.Comment.id == comment_id).first()
        return {'value':result[0], 'comment':comment.text}
    except:
        return {}


def inc_node_to_node_value(db: Session, left_node_id: int, right_node_id: int):
    data = get_node_to_node_data(db, left_node_id, right_node_id)
    if not 'value' in data:
        return
    up = update(models.node_to_node) \
        .where(
            and_(
                models.node_to_node.c.left_node_id == left_node_id,
                models.node_to_node.c.right_node_id == right_node_id
            )) \
        .values( value = data['value'] + 1 )
    db.execute(up)



def get_node_to_node_comment(db: Session, left_node_id: int, right_node_id: int):
    s = select([models.node_to_node.c.comment_id]) \
        .where(
            and_(
                models.node_to_node.c.left_node_id == left_node_id,
                models.node_to_node.c.right_node_id == right_node_id
        ))
    result = db.execute(s).fetchone()
    try:
        comment = db.query(models.Comment).filter(models.Comment.id == result[0]).first()
        return comment
    except:
        return None


def update_node_to_node_comment(db: Session, comment_data: schemas.Comment):
    comment = get_node_to_node_comment( db,
                                        comment_data.left_node_id,
                                        comment_data.right_node_id)
    if comment.text != comment_data.comment:
        prev_edition = comment.make_history()
        db.add(prev_edition)
        comment.text = comment_data.comment
        db.commit()
    left_node = db_node = db.query(models.Node) \
                            .filter(models.Node.id == comment_data.left_node_id) \
                            .first()
    return {'related_nodes': get_related_nodes(db, left_node)}


def get_node_weights(db: Session, limit=60):
    group_stmt = db.query(models.node_to_node.c.left_node_id, func.count('*')\
                .label('related_count')) \
                .group_by(models.node_to_node.c.left_node_id).subquery()

    q = db.query(models.Node.id, models.Node.name, group_stmt.c.related_count) \
                .outerjoin(group_stmt, models.Node.id==group_stmt.c.left_node_id) \
                .order_by(group_stmt.c.related_count.desc()) \
                .limit(limit)

    nodes = db.execute(q)
    result = []
    for node in nodes:
        result.append([ node[1], node[2], node[0] ])
    return result
